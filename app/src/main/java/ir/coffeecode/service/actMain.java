package ir.coffeecode.service;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class actMain extends AppCompatActivity implements View.OnClickListener{

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_main);
        intent = new Intent(actMain.this,ReadData.class);
        Button btnStart = (Button)findViewById(R.id.btnStart);
        Button btnEnd = (Button)findViewById(R.id.btnEnd);
        btnStart.setOnClickListener(actMain.this);
        btnEnd.setOnClickListener(actMain.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnStart:
                startService(intent);
                break;
            case R.id.btnEnd:
                stopService(intent);
                break;
        }
    }
}
