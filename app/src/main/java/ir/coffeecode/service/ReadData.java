package ir.coffeecode.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class ReadData extends Service {

    Timer timer = new Timer();
    Gson gson;
    Realm realm;

    public ReadData() {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(ReadData.this, "Start Service", Toast.LENGTH_SHORT).show();
        gson = new Gson();
        RealmConfiguration config = new RealmConfiguration.Builder(ReadData.this).build();
        Realm.setDefaultConfiguration(config);

        realm = Realm.getDefaultInstance();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                StringRequest request = new StringRequest(Request.Method.GET, "http://test.coffeecode.ir/api/products",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.i("Result: ", response);
                                /*Student[] students = gson.fromJson(response, Student[].class);
                                for (Student item : students) {
                                    realm.beginTransaction();
                                    realm.copyToRealm(item);
                                    realm.commitTransaction();
                                }*/
                                Log.i("Success", "Success");
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Err", error.getMessage());
                            }
                        }) /*{
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("Apikey","ababc8fe1c546cbf23d8a13fa88ef7fc");
                        return params;
                    }
                }*/;
                Volley.newRequestQueue(ReadData.this).add(request);
            }
        }, 0, 5000);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(ReadData.this, "End Service", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
}
