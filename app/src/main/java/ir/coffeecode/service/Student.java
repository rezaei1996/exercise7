package ir.coffeecode.service;

import io.realm.RealmObject;

/**
 * Created by samen06 on 9/21/2016.
 */
public class Student extends RealmObject {
    private String _Name;

    public String getName() {
        return _Name;
    }

    public void setName(String Name) {
        _Name = Name;
    }
}
